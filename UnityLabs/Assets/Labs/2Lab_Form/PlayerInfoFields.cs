using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfoFields : MonoBehaviour
{
    [SerializeField] private FormView _formView;
    [SerializeField] private GameObject _canvas;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (!_canvas.activeInHierarchy)
            {
               _canvas.SetActive(true);
            }
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (_formView.NickField.text == "")
            {
                _formView.EmptyNameWarning();
            }
            else
            {
                _formView.UpdateFields();
                _canvas.SetActive(false);
            }
        }
    }
}
