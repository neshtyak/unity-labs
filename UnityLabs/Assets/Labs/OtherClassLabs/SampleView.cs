﻿using UnityEngine;
using UnityEngine.EventSystems;
using Image = UnityEngine.UI.Image;

public class SampleView : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private Image _image;
    private SampleViewController _sampleViewController;

    private void Awake()
    {
        _sampleViewController = new SampleViewController();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _sampleViewController.OnClick();
    }
}

public class SampleViewController
{
    public void OnClick()
    {
        
    }
}
