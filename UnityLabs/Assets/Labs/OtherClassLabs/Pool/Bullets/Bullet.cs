using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private Pool _pool;
    [SerializeField] private Player_Input _input;
    private GameObject _bullet;
    public int ForceStrength;
    public int seconds;

    public void BulletActive(Vector3 position)
    {
        _bullet = _pool.GetPoolItem("PoolItem");
        //  _bullet.transform.position = _input.pos;
        
    }

    public void BulletForce()
    {
      //  _bullet.GetComponent<Rigidbody>().AddForce(Vector3.forward * ForceStrength);
        StartCoroutine(LateCall(seconds));
    }

    IEnumerator LateCall(int seconds)
    {
        if (_bullet.activeInHierarchy)
            _bullet.SetActive(false);

        yield return new WaitForSeconds(seconds);
    }
}
