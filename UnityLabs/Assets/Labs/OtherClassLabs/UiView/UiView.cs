using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UiView : MonoBehaviour
{
    [SerializeField] private ResourceVisual _resourceVisual;

    [SerializeField] private TextMeshProUGUI _humanText;
    [SerializeField] private TextMeshProUGUI _foodText;
    [SerializeField] private TextMeshProUGUI _woodText;

    private void Start()
    {
        //_resourceVisual = _humanText.GetComponent<ResourceVisual>();

        _resourceVisual.UpdateText(_humanText);
    }
}
