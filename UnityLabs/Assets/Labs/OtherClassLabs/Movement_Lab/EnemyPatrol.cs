using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyPatrol : MonoBehaviour
{
    // private const float EPSILON = 0.001f;
    private const float EPSILON = 0.1f;
    [SerializeField] private NavMeshAgent _navMeshAgent;
    [SerializeField] private Transform[] _waypoints;


    private void Awake()
    {
        _navMeshAgent.SetDestination(_waypoints[0].position);
    }

    private void Update()
    {
        //Проверяем, где находится игрок, если ближе, чем epsilon от точки, то задаем следующую

        if (Vector3.Distance(_waypoints[0].position, _navMeshAgent.transform.position) <= EPSILON)
        {
            _navMeshAgent.SetDestination(_waypoints[1].position);
        }
        if (Vector3.Distance(_waypoints[1].position, _navMeshAgent.transform.position) <= EPSILON)
        {
            _navMeshAgent.SetDestination(_waypoints[0].position);
        }
    }
}
