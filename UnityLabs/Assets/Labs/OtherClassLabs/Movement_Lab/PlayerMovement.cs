using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private NavMeshAgent _navMeshAgent;
    [SerializeField] private PlayerAnimatorFacade _playerAnimatorFacade;
    [SerializeField] private Health _health;

    public void Move(Vector3 destination) => 
        _navMeshAgent.SetDestination(destination);

    public void Update()
    {
        Vector3 movementVector = _navMeshAgent.velocity;
        float speed = Mathf.Abs(movementVector.z);
        _playerAnimatorFacade.SetSpeed(speed);

        int health = _health._healthCount;
        _playerAnimatorFacade.HealthCounter(health);
    }
}
