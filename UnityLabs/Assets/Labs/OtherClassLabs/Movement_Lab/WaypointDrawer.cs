using UnityEngine;
using System.Collections;

public class WaypointDrawer : MonoBehaviour
{
    [SerializeField, Range(0, 10)] private float _radius;
    [SerializeField] private Color _color;

    private void OnDrawGizmos()
    {
        Gizmos.color = _color;
        Gizmos.DrawSphere(transform.position, _radius);
    }
}
