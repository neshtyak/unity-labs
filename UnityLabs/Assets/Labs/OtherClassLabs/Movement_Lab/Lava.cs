using System.Collections;
using UnityEngine;

public class Lava : MonoBehaviour
{
    [SerializeField] private int _damage;
    private Health _health;
    
    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent(out _health))
        {
            Coroutine coroutine = StartCoroutine(DealDamage(_health));
        }
    }

    private IEnumerator DealDamage(Health health)
    {
        yield return new WaitForSecondsRealtime(1);
        health.ApplyDamage(_damage);
    }
}
