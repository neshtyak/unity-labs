using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameStates
{
    public class GameStateMachine : IGameStateMachine
    {
        private IEnterableState _currentState;

        public void ChangeState(IEnterableState state)
        {
            _currentState?.OnExit();
            _currentState = state;
            _currentState?.OnEnter();
        }
    }
}
