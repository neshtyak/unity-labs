using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameStates
{
    public interface IGameStateMachine
    {
        void ChangeState(IEnterableState state);
    }
}
