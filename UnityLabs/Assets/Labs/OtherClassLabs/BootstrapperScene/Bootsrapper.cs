using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameStates;

public class Bootsrapper : MonoBehaviour
{
    private GameGame _gameGame;
    private GameStateMachine _gameStateMachine;
    private SceneLoader _sceneLoader;

    private void Awake()
    {
        _gameStateMachine = new GameStateMachine();
        _gameGame = new GameGame(_gameStateMachine);
        _gameStateMachine.ChangeState(new LoadLevelState(_sceneLoader));
    }
}