using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : IStrategy
{
    public void Perform(Transform transform)
    {
        transform.Rotate(Time.deltaTime, 0, 0);
    }
}
