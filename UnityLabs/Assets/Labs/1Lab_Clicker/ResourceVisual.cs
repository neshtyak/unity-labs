using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;

public class ResourceVisual : MonoBehaviour
{
    public GameResource GameResource;
    [SerializeField] private TextMeshProUGUI _resourceText;
    [SerializeField] private ResourceBank _resourceBank;

    private void Start()
    {
        _resourceBank = Game.Intance.ResourceBank;
        UpdateText(_resourceText);

    }
    public void UpdateText(TextMeshProUGUI _resourceText)
    {
        _resourceText.text = _resourceBank.GetResource(GameResource).ToString();
    }
}
