using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using TMPro;

public class ProductionBuilding : MonoBehaviour
{

    [SerializeField] private GameResource _gameResource;
    [SerializeField] private TextMeshProUGUI _resourceText;
    [SerializeField] private ResourceBank _resourceBank;

    [SerializeField] private Button _button;
    [SerializeField] private Slider _slider;
    [SerializeField] private int _productionTime;
    [SerializeField] private float _addValue;

    private void Start()
    {
        _resourceBank = Game.Intance.ResourceBank;
    }

    public void AddValue()
    {
        _resourceBank.ChangeResource(_gameResource, 1);
        _resourceText.text = _resourceBank.GetResource(_gameResource).ToString();

        StartCoroutine(Timer(_productionTime, _addValue));
    }
    
    private IEnumerator Timer(int cooldown, float add)
    {
        _button.interactable = false;
        while (_slider.value < 1)
        {
            _slider.value += add;
            yield return new WaitForSecondsRealtime(cooldown);
        }
        _button.interactable = true;
        _slider.value = 0f;
    }
}
